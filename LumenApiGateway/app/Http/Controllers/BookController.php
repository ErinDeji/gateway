<?php

namespace App\Http\Controllers;

use App\Book;
use App\Services\AuthorService;
use App\Services\BookService;
use App\Traits\ApiResponsor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookController extends Controller
{
    use ApiResponsor;

    /**
     * The book to consume the books microservice
     * @var BookService
     */

    public $bookService;

    /**
     * The authorto consume the books microservice
     * @var authorervice
     */

    public $authorService;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        BookService $bookService,
        AuthorService $authorService
    ) {
        $this->bookService = $bookService;
        $this->authorService = $authorService;
    }

    /**
     * Return full list of books
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse($this->bookService->obtainBooks());
    }

    /**
     * Create one new Book
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorService->obtainAuthor($request->author_id);

        return $this->successResponse($this->bookService->createBook(
            $request->all(),
            Response::HTTP_CREATED
        ));
    }

    /**
     * Obtains and show an existing one Book
     *@return Illuminate\Http\Response
     */
    public function show($book)
    {
        return $this->successResponse($this->bookService->obtainBook($book));
    }

    /**
     * Update an existing Book
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $book)
    {
        return $this->successResponse($this->bookService->updateBook(
            $request->all(),
            $book
        ));
    }

    /**
     *  Delete an existing Book with id
     *@return Illuminate\Http\Response
     */
    public function destroy($book)
    {
        return $this->successResponse($this->bookService->deleteBook(
            $book
        ));
    }
    //
}