<?php

namespace App\Http\Controllers;

use App\Author;
use App\Services\AuthorService;
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class AuthorController extends Controller
{
    use ApiResponsor;

    /**
     * The service to consume the authors microservice
     * @var AuthorService
     */

    public $authorService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * Return full list of Authors
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse($this->authorService->obtainAuthors());
    }

    /**
     * Create one new Author
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->successResponse($this->authorService->createAuthor(
            $request->all(),
            Response::HTTP_CREATED
        ));
    }

    /**
     * Obtains and show an existing one Author
     *@return Illuminate\Http\Response
     */
    public function show($author)
    {
        return $this->successResponse($this->authorService->obtainAuthor($author));
    }

    /**
     * Update an existing Author
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $author)
    {
        return $this->successResponse($this->authorService->updateAuthor(
            $request->all(),
            $author
        ));
    }

    /**
     *  Delete an existing Author with id
     *@return Illuminate\Http\Response
     */
    public function destroy($author)
    {
        return $this->successResponse($this->authorService->deleteAuthor(
            $author
        ));
    }
    //
}